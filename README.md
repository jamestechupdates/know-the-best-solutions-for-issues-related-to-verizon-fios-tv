# Know the Best Solutions for Issues Related to Verizon FiOS TV#

Many times, the Verizon FiOS TV users face different problems such as the Set-Top box is not working in a proper manner or remote control is not responding. Such issues are common, and therefore, one can easily get rid of them by following the general troubleshooting steps.
In here, you will find the steps that you can follow to resolve the issues like DVR isn't working, there are no picture or batteries issues.
What to do when Verizon Set-Top box or DVR is not working?

You can get rid of the most common issues like missing On Demand menu or Interactive Media Guide, the Set-Top box is not responding, or DVR recording is not working, all by resetting your Set-Top box or router. To do so, follow:

●	At first, unplug the power cord of the router and wait for about 15 seconds before plugging it back into the main power outlet.

●	Now, wait for another 30 seconds to check whether this has resolved the occurring issue or not. If not, continue with the further steps.

●	Unplug the power cord of the Verizon Set-Top box and wait for about 15 seconds. After that, plug it back into the main power outlet.

●	Next, you have to wait for the Set-Top box to turn on.

●	This will update the Interactive Media Guide (IMG) and you may have to wait for a couple of minutes.

●	At last, check whether your problem is resolved or not.

What to do when there is no Picture on your Verizon FiOS TV?

In most of the cases, when Verizon FiOS TV users complain about no Picture problem, they were facing connection or power issues. Therefore, follow the given steps:

●	Ensure your FiOS TV is plugged in. For this, press the power button on your television and turn it on.

●	If your FiOS television is plugged into a power outlet which is controlled by a wall switch, make sure you have turned on the switch.

●	Now, with your remote control, set the channel of your FiOS TV to 3 or 4.

●	Next, unplug the television cord and re-plug it to check whether the problem is with your FiOS TV or the Set-Top box.

●	Ensure that the Set-top box is plugged in properly and turned on.

●	Also, verify that you are not having power outages. Therefore, check other electrical equipment if they are working.

What to do when your Verizon FiOS television remote control is not working?

Check for the batteries of your television's remote control if they are drained or are dislodged:

●	Verify that the batteries are seated properly. Therefore, open the back cover of your remote and reset the batteries.

●	Press a few keys on your remote control, like DVD, AUX, TV, STB. If the top light of the controller briefly flashes, the batteries are absolutely fine. If it doesn't flash, it is time to replace the batteries.

For the case, these steps do not resolve your problem or you have other queries related to Verizon FiOS TV, contact the Verizon Customer Support toll-free helpline number (**[icustomerservice](https://icustomerservice.net/verizon-customer-service/)**) right away.